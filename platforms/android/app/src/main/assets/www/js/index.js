/**
 * Copyright 2016 IBM Corp.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var Messages = {};
var wlInitOptions = {};


var collectionName = 'people';
var collections = {};
var options = {};
collections[collectionName] = {};
collections[collectionName].searchFields = {name: 'string', age: 'integer'};


function wlCommonInit(){

    document.getElementById("initCollection").addEventListener("click", initCollection, false);
    // document.getElementById("initSecuredCollection").addEventListener("click", function(){ initCollection("secured"); }, false);

    document.getElementById("findByName").addEventListener("click", findByName, false);

    document.getElementById("myInput").onkeyup = function() {
        // document.getElementById("display").innerHTML = this.value;
        var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        ul = document.getElementById("myUL");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            txtValue = a.textContent || a.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    };




    // var obj = document.getElementById("api_select");
    // obj.addEventListener("change", function() {
    //     // Add Document
    //     if(obj.selectedIndex == 1){
    //         // showHideConsole("hide");
    //         displayDiv("AddDataDiv");
    //     }
    //     // Find Document
    //     else if(obj.selectedIndex == 2) {
    //         // showHideConsole("show");
    //         displayDiv("FindDocDiv");
    //     }
    //
    // });
}


// function buildSelectOptions(obj){
//     obj.options[1] = new Option("Add Data");
//     obj.options[2] = new Option("Find Document");
// }


// function displayDiv(divName){
//     var divNames = ["AddDataDiv", "FindDocDiv", "ReplaceDocDiv", "RemoveDocDiv", "AdapterIntegrationDiv", "ChangePasswordDiv"];
//     for(i=0; i<divNames.length; i++){
//         document.getElementById(divNames[i]).style.display = "none";
//     }
//     document.getElementById(divName).style.display = "block";
// }




//****************************************************
// initCollection
//****************************************************
function initCollection(){


    WL.JSONStore.init(collections, options).then(function () {

        // buildSelectOptions(document.getElementById("api_select"));
        document.getElementById("initCollection_screen").style.display = "none";
        document.getElementById("apiCommands_screen").style.display = "block";

        var data = {};
        var options = {};


        for (var i = 0; i <= 10; i++){
            console.log(i);
            data.name = "abc"+i;
            data.age = i;
            // alert(data.name);
            find(data);

        }
        function find(data) {
            try {
                WL.JSONStore.get(collectionName).add(data, options).then(function () {
                    // showHideConsole("show");
                    document.getElementById("resultsDiv").innerHTML = "New Document Added Successfuly<br>Name: " + data.name + " | Age: " + data.age;
                }).fail(function (errorObject) {
                    // showHideConsole("show");
                    document.getElementById("resultsDiv").innerHTML = "Failed to Add Data";
                });
            } catch (e) {
                alert("WL.JSONStore Add Data Failure");
            }
            document.getElementById("addName").value = "";
            document.getElementById("addAge").value = "";
        } //50sec


    })
        .fail(function (errorObject) {
            alert("Filed to initialize collection\n"+ JSON.stringify(errorObject));
        });
}



//****************************************************
// findByName
//****************************************************
function findByName(){

    var html ='';
        WL.JSONStore.get(collectionName).findAll(options).then(function (res) {
            document.getElementById("resultsDiv").innerHTML = JSON.stringify(res);
            res.forEach(function (arrayItem) {
                var x = arrayItem.json.name;
                html += '<li class="nav-item"><a class="nav-link" ><div class="item-content-link"><div class="icon bg-purple-1 color-purple"><i class="ri-user-settings-line"></i></div><h3 class="link-title">'+x+'</h3></div></a></li>';
                console.log(x);
            });
            $("#myUL").append(html);
		}).fail(function (errorObject) {
            document.getElementById("resultsDiv").innerHTML = errorObject.msg;
		});
}



